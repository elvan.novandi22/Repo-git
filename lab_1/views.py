from django.shortcuts import render
from datetime import datetime

# Enter your name here
mhs_name = 'Elvan Rizky Novandi' # TODO Implement this
mhs_age= 1997
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'umur': calculate_age(mhs_age)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    today=  datetime.now().year
    return today-birth_year
